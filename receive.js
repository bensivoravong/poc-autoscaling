/**
 * Constantly polls an SQS queue, 'handling' and deleting the messages.
 * Uses long-polling with a timeout of 20 seconds.
 */
var AWS = require('aws-sdk');

AWS.config.update({
    region: 'us-east-1',
});

var sqs = new AWS.SQS({
    apiVersion: '2012-11-05'
});

var queueURL = process.env.SQS_URL;

async function handleMessage() {
    var params = {
        AttributeNames: [
            "SentTimestamp"
        ],
        MaxNumberOfMessages: 1,
        MessageAttributeNames: [
            "All"
        ],
        QueueUrl: queueURL,
        WaitTimeSeconds: 20
    };

    return new Promise((resolve, reject) => {
        sqs.receiveMessage(params, function (err, data) {
            if (err) {
                console.log("Receive Error", err);
                reject();
            } else if (data.Messages) {
                console.log('Message: ', data.Messages[0].MessageAttributes.ID.StringValue, ' : ', data.Messages[0].Body);
                var deleteParams = {
                    QueueUrl: queueURL,
                    ReceiptHandle: data.Messages[0].ReceiptHandle
                };
                sqs.deleteMessage(deleteParams, function (err, d) {
                    if (err) {
                        console.log("Delete Error", err);
                        reject()
                    } else {
                        console.log('Message Deleted: ', data.Messages[0].MessageAttributes.ID.StringValue, ' : ', data.Messages[0].Body);
                    }
                    resolve();
                });
            } else {
                console.log('No Messages');
                resolve();
            }
        });
    });
}

function poll() {
    handleMessage()
    .then(res => {
        poll();
    })
    .catch(err => {
    })
}

poll();