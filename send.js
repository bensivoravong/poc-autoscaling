var AWS = require('aws-sdk');

AWS.config.update({
    region: 'us-east-1',
});

var sqs = new AWS.SQS({
    apiVersion: '2012-11-05'
});

var queueURL = process.env.SQS_URL;

function send() {

    var meta = new AWS.MetadataService();
    meta.request("/latest/meta-data/instance-id", function (err, data) {
        var instanceID = data ? data : 'localhost';

        var rand = '' + (Math.round(Math.random() * 1000001));

        var params = {
            MessageAttributes: {
                ID: {
                    DataType: 'String',
                    StringValue: instanceID
                }
            },
            MessageBody: rand,
            QueueUrl: queueURL,
        };

        sqs.sendMessage(params, (err, data) => {
            if (err) {
                console.log("Error", err);
            } else {
                console.log("Success: ", instanceID, ' : ', rand);
            }
        });
    });
}

for (let i = 0; i<5; i++) {
    send()
}